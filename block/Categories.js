import React from "react";
import { StyleSheet, FlatList, Text, View, Image } from "react-native";
import { products } from "../data";
console.log(products);
export default class Categories extends React.Component {
  renderItem = ({ item, index }) => {
    console.log(item);
    return (
      <View style= {styles.Product}>

        <Text>{item.name}</Text>
        <Text>{item.salePrice}</Text>
        <Text>{item.shortDescription}</Text>
        <Text>{item.originalPrice}</Text>
        <Image 
        style={styles.image}


        resizeMode="contain" source={{ uri: item.image }} 

        
        />
      </View>
    );
  };

  render() {
    return (
      <View>
        <FlatList
          data={products}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  
image: {
    width: 120,
    height: 120,
    borderRadius: 0,
  },
    
Product: {
  borderWidth :10,
  
}
});

