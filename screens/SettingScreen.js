import React from 'react';
import { StyleSheet,Text, View} from 'react-native';

export default class SettingScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text >Setting Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:20
  }
});