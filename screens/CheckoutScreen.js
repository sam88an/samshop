import React from 'react';
import { StyleSheet,Text, View} from 'react-native';

export default class CheckoutScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text >Check Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:20
  }
});