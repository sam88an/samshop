import React from 'react';
import { StyleSheet,Text, View} from 'react-native';

export default class OrderScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text >Order Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:20
  }
});