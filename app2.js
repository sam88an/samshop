import React from 'react';
import {FlatList,ScrollView, StyleSheet, TextInput,Text, View ,Button, Image , Icon} from 'react-native';
import Promotion from './block/Promotion';
import Footer from './block/Footer';
import Categories from './block/Categories';


export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.menu}>
          </View>
          <View style={styles.searchBar}>
              <TextInput style={styles.searchBar}   />
          </View>
          <View style={styles.searchButton}></View>
        </View >

        <View style={styles.body} >
        <ScrollView>
          <Categories/>
          <Promotion/>
       </ScrollView>
        </View>
      
        <View style={styles.footer} >
          <Footer/>
        </View>
       

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    width:'100%',
    marginTop:20,
    backgroundColor:'blue',
    flexDirection: 'row',
    height:50,
    

  },
  menu: { 
    borderWidth:1,
    width:50,
    padding:5,

  },
    searchBar: {
    flex:1,
    height:50,
    borderWidth:1,
    flexDirection: 'row',
    backgroundColor:'white'

  },
  searchBarInputText: {
    height:50,
    borderWidth:1,
    justifyContent: 'center',
    
    
  },
  searchButton: {
    height:50,
    borderWidth:1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width:50
  },
  btSearch: {
    height:50,
    borderWidth:1,
    width:50,
  
  },
  ScrollViewBody:{
    width:'100%'
  },
  body: {
    flex:1,
    width :'100%',
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    width :'100%',
    borderWidth:1,
    height:50,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
